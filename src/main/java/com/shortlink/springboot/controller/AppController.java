package com.shortlink.springboot.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AppController {

    @Autowired
    private Converter converter;

    @RequestMapping(value = "/go{ref}", method = RequestMethod.GET)
    public ResponseEntity<String> go(@RequestParam(value = "ref", required = true) String ref) {
        String url = converter.expandURL(ref);
        if (url != null) {
            return new ResponseEntity<>(url, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @RequestMapping(value = "/get{ref}", method = RequestMethod.POST)
    public ResponseEntity<String> save(@RequestParam(value = "ref", required = true) String ref) {
        String url = converter.shortenURL(ref);
        if (url != null) {
            return new ResponseEntity<>(url, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }
}
