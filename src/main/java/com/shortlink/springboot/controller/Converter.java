package com.shortlink.springboot.controller;

import com.shortlink.springboot.entity.TagLink;
import com.shortlink.springboot.dao.TagLinkDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.Random;

@Service
@Scope("singleton")
public class Converter {

    @Autowired
    private TagLinkDao linkDao;

    private String domain;
    private char myChars[];

    private Random myRand;
    private int keyLength;

    public Converter() {
        myRand = new Random();
        keyLength = 8;
        myChars = new char[62];
        for (int i = 0; i < 62; i++) {
            int j = 0;
            if (i < 10) {
                j = i + 48;
            } else if (i > 9 && i <= 35) {
                j = i + 55;
            } else {
                j = i + 61;
            }
            myChars[i] = (char) j;
        }
        domain = "http://localhost:8080";
    }

    public Converter(int length, String newDomain) {
        this();
        this.keyLength = length;
        if (!newDomain.isEmpty()) {
            newDomain = sanitizeURL(newDomain);
            domain = newDomain;
        }
    }

    public void setTagLinkDao(TagLinkDao linkDao){
        this.linkDao = linkDao;

    }

    public synchronized String shortenURL(String longURL) {
        String shortURL = "";
        if (validateURL(longURL)) {
            longURL = sanitizeURL(longURL);
            TagLink tag = linkDao.findByLong(longURL);
            if (tag != null) {
                shortURL = domain + "/" + tag.getKey();
            } else {
                shortURL = domain + "/" + getKey(longURL);
            }
        }
        return shortURL;
    }

    public synchronized String expandURL(String shortURL) {
        String longURL = "";
        String key = shortURL.substring(domain.length() + 1);
        TagLink tag = linkDao.findByKey(key);
        longURL = tag.getOriginalUrl();
        return longURL;
    }

    boolean validateURL(String url) {
        return true;
    }

    String sanitizeURL(String url) {
        if (url.substring(0, 7).equals("http://"))
            url = url.substring(7);

        if (url.substring(0, 8).equals("https://"))
            url = url.substring(8);

        if (url.charAt(url.length() - 1) == '/')
            url = url.substring(0, url.length() - 1);
        return url;
    }

    private String getKey(String longURL) {
        String key = generateKey();
        linkDao.add(new TagLink(longURL, key));

        return key;
    }

    private String generateKey() {
        String key = "";
        boolean flag = true;
        while (flag) {
            key = "";
            for (int i = 0; i <= keyLength; i++) {
                key += myChars[myRand.nextInt(62)];
            }
            if (!linkDao.isExistKey(key)) {
                flag = false;
            }
        }
        return key;
    }

}