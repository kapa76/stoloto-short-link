package com.shortlink.springboot.dao;

import com.shortlink.springboot.entity.TagLink;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@Repository
@Transactional
public class TagLinkDao {
    @Autowired
    private SessionFactory sessionFactory;

    public TagLinkDao() {
    }

    public void setSessionFactory(SessionFactory sessionFactory){
        this.sessionFactory = sessionFactory;
    }

    public boolean isExistKey(String key) {
        return findByKey(key) != null;
    }

    @Transactional(propagation = Propagation.MANDATORY, rollbackFor = TransactionException.class)
    public void add(TagLink tagLink) {
        Session session = this.sessionFactory.getCurrentSession();
        session.persist(tagLink);
    }

    public TagLink findByKey(String key) {
        Session session = this.sessionFactory.getCurrentSession();
        return (TagLink) session.createQuery("select t from TagLink t where t.key = ?").setParameter(0, key).uniqueResult();
    }

    public TagLink findByLong(String longURL) {
        Session session = this.sessionFactory.getCurrentSession();
        return (TagLink) session.createQuery("select t from TagLink t where t.originalUrl = ?").setParameter(0, longURL).uniqueResult();
    }
}
