package com.shortlink.springboot.entity;

import javax.persistence.*;

@Entity
@Table(name = "tag_link")
public class TagLink {

    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "original_url", length = 512, unique = true, nullable = false)
    private String originalUrl;

    @Column(name = "key", length = 20, unique = true, nullable = false)
    private String key;

    public TagLink(){}

    public TagLink(String longUrl, String key) {
        this.originalUrl = longUrl;
        this.key = key;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOriginalUrl() {
        return originalUrl;
    }

    public void setOriginalUrl(String originalUrl) {
        this.originalUrl = originalUrl;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
